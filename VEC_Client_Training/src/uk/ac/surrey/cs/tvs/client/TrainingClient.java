/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.client;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Provides a single client object for calling the various functions of the client. These include Key Generation, CSR generation,
 * Certificate importing and the full ballot generation and audit process.
 * 
 * Any UI should create an instance of this class to call the various methods on. The processes involved need to be called in a
 * particular order. For example, before a certificate can be imported a Certificate Signing Request should have been generated.
 * This class will eventually enforce the order of calling for these methods.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class TrainingClient {

  /**
   * ConfigFile for this client that specifies paths to various files generated
   */
  private ConfigFile          conf;

  /**
   * Logger
   */
  private static final Logger logger        = LoggerFactory.getLogger(TrainingClient.class);

  /**
   * Schema used for config file.
   */
  private static final String CONFIG_SCHEMA = "configschema.json";

  private static final String BASE_PATH     = "/sdcard/trainingmode/";
  /**
   * ClientType that represents whether this is a VPS,EVM,CANCEL client
   */
  private ClientType             type;
  /**
   * Creates an instance of client using the ConfigFile located at the path specified
   * 
   * @param configFile
   *          string path to ConfigFile
   * @throws JSONIOException
   * @throws IOException
   * @throws PeerSSLInitException
   * @throws MaxTimeoutExceeded
   */
  public TrainingClient(String configFile) throws JSONIOException, IOException, PeerSSLInitException, MaxTimeoutExceeded {
    super();

    logger.info("Created Client object with Config:{}", configFile);
    // this.conf = new ConfigFile(configFile, CONFIG_SCHEMA); //TODO Re-enable schema checking
    this.conf = new ConfigFile(configFile);// , BASE_PATH+CONFIG_SCHEMA);
    this.type = ClientType.valueOf(this.conf.getStringParameter(ClientConfig.PURPOSE));
  }
  /**
   * Gets the ClientType of the current instance
   * 
   * @return ClientType of this client
   */
  public ClientType getType() {
    return this.type;
  }
  /**
   * Gets the ConfigFile representing the client configuration
   * 
   * @return ConfigFile for the client configuration
   */
  public ConfigFile getClientConf() {
    return this.conf;
  }

}
