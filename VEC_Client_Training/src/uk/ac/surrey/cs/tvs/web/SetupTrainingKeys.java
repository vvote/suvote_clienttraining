package uk.ac.surrey.cs.tvs.web;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSKeyPair;


public class SetupTrainingKeys {

  public SetupTrainingKeys() {
    // TODO Auto-generated constructor stub
  }

  public static void main(String[] args) throws KeyStoreException, IOException {
    BLSKeyPair mbbKP = BLSKeyPair.generateKeyPair();
    BLSKeyPair evmKP = BLSKeyPair.generateKeyPair();
    BLSKeyPair vpsKP = BLSKeyPair.generateKeyPair();
    BLSKeyPair cancelAuthKP = BLSKeyPair.generateKeyPair();
    TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
    tvsKeyStore.addBLSKeyPair(mbbKP, "MBB");
    tvsKeyStore.addBLSKeyPair(evmKP, "EVM");
    tvsKeyStore.addBLSKeyPair(vpsKP, "VPS");
    tvsKeyStore.addBLSKeyPair(cancelAuthKP, "CancelAuth");
    tvsKeyStore.store("./demo/TestDeviceOne/trainingKeystore.bks");
  }

}
