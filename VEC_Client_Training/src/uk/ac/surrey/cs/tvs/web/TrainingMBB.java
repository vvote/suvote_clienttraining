package uk.ac.surrey.cs.tvs.web;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.AuditMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.PODMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.StartEVMMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;

public class TrainingMBB {

  private static final TrainingMBB      _instance   = new TrainingMBB();
  private HashMap<String, MessageState> serialState = new HashMap<String, MessageState>();

  private TrainingMBB() {

  }

  public static TrainingMBB getInstance() {
    return _instance;
  }

  public String signPODMessage(JSONObject msg) {
    try {
      serialState.put(msg.getString(PODMessage.ID), MessageState.POD);
      TVSSignature signature = new TVSSignature(SignatureType.BLS, TrainingKeyStore.getInstance().getMBBPrivateKey());
      signature.update(msg.getString(PODMessage.ID));
      signature.update(msg.getString(PODMessage.DISTRICT));
      return signature.signAndEncode(EncodingType.BASE64);

    }
    catch (TVSKeyStoreException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (TVSSignatureException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (JSONException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return "ERROR";
  }
  public String signCancelAuthMessage(JSONObject msg) {
    try {
      TVSSignature signature = new TVSSignature(SignatureType.BLS, TrainingKeyStore.getInstance().getCancelAuthPrivateKey());
      signature.update(msg.getString(PODMessage.ID));
      signature.update(msg.getString(PODMessage.DISTRICT));
      return signature.signAndEncode(EncodingType.BASE64);

    }
    catch (TVSKeyStoreException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (TVSSignatureException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (JSONException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return "ERROR";
  }

  public String signStartEVMMessage(JSONObject msg) {
    try {
      serialState.put(msg.getString(PODMessage.ID), MessageState.STARTEVM);

      TVSSignature signature = new TVSSignature(SignatureType.BLS, TrainingKeyStore.getInstance().getMBBPrivateKey());
      signature.update("startevm");

      signature.update(msg.getString(StartEVMMessage.ID));
      signature.update(msg.getString(StartEVMMessage.DISTRICT));
      return signature.signAndEncode(EncodingType.BASE64);
    }
    catch (TVSKeyStoreException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (TVSSignatureException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (JSONException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return "ERROR";
  }
  public String signCancelMessage(JSONObject msg) {
    try {
      serialState.put(msg.getString(PODMessage.ID), MessageState.CANCEL);

      TVSSignature signature = new TVSSignature(SignatureType.BLS, TrainingKeyStore.getInstance().getMBBPrivateKey());
      signature.update("cancel");

      signature.update(msg.getString(StartEVMMessage.ID));
      return signature.signAndEncode(EncodingType.BASE64);
    }
    catch (TVSKeyStoreException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (TVSSignatureException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (JSONException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return "ERROR";
  }

  public String signAuditMessage(JSONObject msg, JSONArray ballotReductions) throws TVSKeyStoreException, TVSSignatureException,
      JSONException {
    serialState.put(msg.getString(PODMessage.ID), MessageState.POD);
    TVSSignature signature = new TVSSignature(SignatureType.BLS, TrainingKeyStore.getInstance().getMBBPrivateKey());
    signature.update("audit");
    signature.update(msg.getString(AuditMessage.ID));
    StringBuffer reducedPerms = new StringBuffer();

    for (int raceCount = 0; raceCount < ballotReductions.length(); raceCount++) {
      reducedPerms.append(ballotReductions.getJSONArray(raceCount).join(MessageFields.PREFERENCE_SEPARATOR));
      reducedPerms.append(MessageFields.RACE_SEPARATOR);
    }
    signature.update(reducedPerms.toString());
    return signature.signAndEncode(EncodingType.BASE64);

  }

  public boolean serialNoUsed(String serialNo) {
    return this.serialState.containsKey(serialNo);
  }

  public MessageState serialNoState(String serialNo) {
    MessageState returnState = MessageState.UNUSED;
    if (this.serialState.containsKey(serialNo)) {
      returnState = this.serialState.get(serialNo);
    }
    return returnState;
  }
}
