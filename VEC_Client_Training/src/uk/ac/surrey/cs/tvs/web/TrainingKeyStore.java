package uk.ac.surrey.cs.tvs.web;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPrivateKey;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPublicKey;


public class TrainingKeyStore {

  private static final TrainingKeyStore _trainingKeyStore = new TrainingKeyStore();
  private TVSKeyStore keyStore;
  public static TrainingKeyStore getInstance(){
    return _trainingKeyStore;
  }
  private TrainingKeyStore() {
    
    try {
      keyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      keyStore.load("/sdcard/trainingmode/trainingKeystore.bks", "".toCharArray());
      //keyStore.load(this.getClass().getResourceAsStream("trainingKeystore.bks"), "".toCharArray());
    }
    catch (KeyStoreException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
   
    
  }

  public BLSPublicKey getVPSPublicKey() throws TVSKeyStoreException{
    return keyStore.getBLSPublicKey("VPS");
  }
  public BLSPublicKey getMBBPublicKey() throws TVSKeyStoreException{
    return keyStore.getBLSPublicKey("MBB");
  }
  public BLSPublicKey getEVMPublicKey() throws TVSKeyStoreException{
    return keyStore.getBLSPublicKey("EVM");
  }
  public BLSPrivateKey getVPSPrivateKey() throws TVSKeyStoreException{
    return keyStore.getBLSPrivateKey("VPS");
  }
  public BLSPrivateKey getMBBPrivateKey() throws TVSKeyStoreException{
    return keyStore.getBLSPrivateKey("MBB");
  }
  public BLSPrivateKey getEVMPrivateKey() throws TVSKeyStoreException{
    return keyStore.getBLSPrivateKey("EVM");
  }
  
  public BLSPrivateKey getCancelAuthPrivateKey() throws TVSKeyStoreException{
    return keyStore.getBLSPrivateKey("CancelAuth");
  }
}
