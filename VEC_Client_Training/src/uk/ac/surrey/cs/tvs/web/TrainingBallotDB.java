package uk.ac.surrey.cs.tvs.web;

import java.io.BufferedReader;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.prng.BasicEntropySourceProvider;
import org.bouncycastle.crypto.prng.EntropySource;
import org.bouncycastle.crypto.prng.FixedSecureRandom;
import org.bouncycastle.crypto.prng.SP800SecureRandom;
import org.bouncycastle.crypto.prng.SP800SecureRandomBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants.UIPODMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

public class TrainingBallotDB {

  
  private JSONObject districtConf;
  public TrainingBallotDB(JSONObject districtConf) throws NoSuchAlgorithmException {
    this.districtConf=districtConf;
  }

  public JSONObject getBallot(String serialNo, String district) throws NoSuchAlgorithmException {
    BufferedReader br = null;
    try {
      
   // Use DRBG from BouncyCastle to prepare source of randomness from hash output
      StringBuffer sb = new StringBuffer();
      for(int i=0;i<10;i++){
        sb.append(serialNo);
      }
      FixedSecureRandom fsr = new FixedSecureRandom(sb.toString().getBytes());
      // We disable predictionResistant since this would require re-seeding and we only have a finite amount of seed randomness
      SP800SecureRandomBuilder rBuild = new SP800SecureRandomBuilder(fsr, false);
      SP800SecureRandom generatePermutation = rBuild.buildHash(new SHA256Digest(), null, false);
      
      JSONObject districtDetails=districtConf.getJSONObject(district);
      ArrayList<ArrayList<Integer>> permutation =this.generateBaseBallotAndPermute(districtDetails,generatePermutation);
      JSONObject ballot = new JSONObject();
      ballot.put(UIPODMessage.SERIAL_NO, serialNo);
      ballot.put(UIPODMessage.DISTRICT, district);
      //ballot.put("sigs", "SomeSignature");
      JSONArray races = new JSONArray();
      JSONObject la = new JSONObject();
      la.put("id", "LA");
      la.put("permutation", permutation.get(0));
      races.put(la);
      JSONObject atl = new JSONObject();
      atl.put("id", "LC_ATL");
      atl.put("permutation", permutation.get(1));
      races.put(atl);
      JSONObject btl = new JSONObject();
      btl.put("id", "LC_BTL");
      btl.put("permutation", permutation.get(2));
      races.put(btl);
      ballot.put("races", races);
      return ballot;   
    }
    catch (JSONException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;
  }

  public ArrayList<ArrayList<Integer>> generateBaseBallotAndPermute(JSONObject district,SecureRandom permGenerator) throws JSONException{
    ArrayList<ArrayList<Integer>> perms = new ArrayList<ArrayList<Integer>>();
    for(int raceCount =0;raceCount<3;raceCount++){
      
      ArrayList<Integer> racePerm = new ArrayList<Integer>();
      String raceString="";
      switch(raceCount){
        case 0:
          raceString="la";
          break;
        case 1:
          raceString="lc_atl";
          break;
        case 2:
          raceString="lc_btl";
          break;
      }
      for(int i=0;i<district.getInt(raceString);i++){
        racePerm.add(i);
      }
      Collections.shuffle(racePerm, permGenerator);
      perms.add(racePerm);
    }
    return perms;
    
  }
  public static void main(String[] args) throws NoSuchAlgorithmException, JSONIOException {
    JSONObject districtConf = IOUtils.readJSONObjectFromFile("/sdcard/trainingmode/districtconf.json");
    TrainingBallotDB ballotDB = new TrainingBallotDB(districtConf);
    System.out.println(ballotDB.getBallot("VPS:1", "Bass"));
    System.out.println(ballotDB.getBallot("VPS:1", "Bass"));
    System.out.println(ballotDB.getBallot("VPS:1", "Bass"));
    System.out.println(ballotDB.getBallot("VPS:1", "Bass"));
    System.out.println(ballotDB.getBallot("VPS:2", "Kilsyth"));
  }
}
