/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException;
import uk.ac.surrey.cs.tvs.comms.exceptions.ConsensusException;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.VVoteWebServer;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants.UIMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * Servlet used to handle audit and cancellation.
 * 
 * @author Chris Culnane
 * 
 */
public class MBBPODProxyServlet implements NanoServlet {

  /**
   * Configuration file for the client.
   */
  private ConfigFile          clientConf;

  /**
   * Holds a reference of the schema for the JSON message this proxy accepts
   */
  private String              schema;

  /**
   * Path to the schema file
   */
  private static String       SCHEMA_PATH = "/sdcard/trainingmode/schemas/mbbpodproxyschema.json";

  /**
   * Configuration file for the district information.
   */
  private JSONObject          districtConf;

  /**
   * Logger
   */
  private static final Logger logger      = LoggerFactory.getLogger(MBBPODProxyServlet.class);

  /**
   * TVSKeyStore that contains the certificates to verify the message from the MBB
   */
  private TVSKeyStore         clientKeyStore;

  private TrainingBallotDB    ballotDB;

  /**
   * Constructor requiring the configuration information for the election and client.
   * 
   * @param client
   *          Client that this MBBPODProxy relates to
   * 
   * @param districtConfFile
   *          String path to district config file
   * 
   * @throws JSONIOException
   * @throws CryptoIOException
   * @throws NoSuchAlgorithmException
   */
  public MBBPODProxyServlet(String districtConfFile) throws JSONIOException, CryptoIOException, NoSuchAlgorithmException {
    super();
    logger.info("Loading MBBPODProxyServlet");
    this.districtConf = IOUtils.readJSONObjectFromFile(districtConfFile);
    this.ballotDB = new TrainingBallotDB(this.districtConf);
    this.schema = JSONUtils.loadSchema(MBBPODProxyServlet.SCHEMA_PATH);
    logger.info("Finished loading MBBPODProxyServlet");
  }

  /**
   * Prepares an audit to be sent to the MBB.
   * 
   * @param submitSig
   *          The submission signature.
   * @param messageSent
   *          The message to be sent.
   * @return JSONArray of ballot reductions
   * @throws JSONException
   * @throws TVSSignatureException
   * @throws JSONIOException
   * @throws NoSuchAlgorithmException
   * @throws ProxyMessageProcessingException
   * @throws BallotFileNotFoundException
   */
  private JSONArray prepareAuditMessage(JSONObject messageSent) throws JSONException, TVSSignatureException, JSONIOException,
      NoSuchAlgorithmException {

    String serialNo = messageSent.getString(ClientConstants.UIMessage.SERIAL_NO);
    logger.info("Preparing audit message for {}", serialNo);

    JSONObject ballot = this.ballotDB.getBallot(serialNo, messageSent.getString(ClientConstants.UIAUDITResponse.DISTRICT));
    System.out.println(ballot.toString());
    JSONArray perms = new JSONArray();
    JSONArray races = ballot.getJSONArray("races");
    perms.put(races.getJSONObject(0).getJSONArray("permutation"));
    perms.put(races.getJSONObject(1).getJSONArray("permutation"));
    perms.put(races.getJSONObject(2).getJSONArray("permutation"));
    return perms;

  }



  /**
   * Processes a servlet message. This proxy only adds/updates data in the original message. It aims to forward the message on with
   * as little change as possible. Hence the cancel message only requires a signature field to be created and nothing else.
   * 
   * @param message
   *          The message to process.
   * @return The message that has been sent to tbe MBB in response to the servlet request.
   * @throws JSONException
   * @throws IOException
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws CertificateException
   * @throws UnrecoverableKeyException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws CryptoIOException
   * @throws TVSSignatureException
   * @throws JSONIOException
   * @throws TVSKeyStoreException 
   * @throws MBBCommunicationException
   * @throws ProxyMessageProcessingException
   * @throws ConsensusException
   */
  private JSONObject processMessage(String message) throws JSONException, NoSuchAlgorithmException, TVSSignatureException,
      JSONIOException, TVSKeyStoreException {
    JSONObject messageSent = new JSONObject(message);

    JSONArray ballotReductions = null;
    String type = messageSent.getString(UIMessage.TYPE);

    if (type.equalsIgnoreCase(MessageTypes.AUDIT)) {
      ballotReductions = this.prepareAuditMessage(messageSent);
      messageSent.put(ClientConstants.UIResponse.WBB_SIGNATURES, TrainingMBB.getInstance().signAuditMessage(messageSent,ballotReductions));
    }
    else if (type.equalsIgnoreCase(MessageTypes.CANCEL)) {
      messageSent.put(ClientConstants.UIResponse.WBB_SIGNATURES, TrainingMBB.getInstance().signCancelMessage(messageSent));
    }

    
    if (ballotReductions != null) {
      // Prepare response data for client
      JSONArray races = new JSONArray();
      JSONObject laRace = new JSONObject();
      JSONObject lcATLRace = new JSONObject();
      JSONObject lcBTLRace = new JSONObject();

      laRace.put(ClientConstants.UIAUDITResponse.RACE_ID, ClientConstants.UIPODResponse.DISTRICT_RACE);
      laRace.put(ClientConstants.UIAUDITResponse.RACE_PERMUTATION, ballotReductions.getJSONArray(0));
      lcATLRace.put(ClientConstants.UIAUDITResponse.RACE_ID, ClientConstants.UIPODResponse.REGION_RACE_ATL);
      lcATLRace.put(ClientConstants.UIAUDITResponse.RACE_PERMUTATION, ballotReductions.getJSONArray(1));
      lcBTLRace.put(ClientConstants.UIAUDITResponse.RACE_ID, ClientConstants.UIPODResponse.REGION_RACE_BTL);
      lcBTLRace.put(ClientConstants.UIAUDITResponse.RACE_PERMUTATION, ballotReductions.getJSONArray(2));

      races.put(laRace);
      races.put(lcATLRace);
      races.put(lcBTLRace);
      messageSent.put(ClientConstants.UIPODResponse.RACES, races);
    }

    return messageSent;

  }

  /**
   * Runs the servlet.
   * 
   * @param uri
   *          String
   * @param method
   *          Method
   * @param header
   *          Map<String,String>
   * @param params
   *          Map<String,String>
   * @param files
   *          Map<String,String>
   * @param homeDir
   *          File
   * @return Response
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.NanoServlet#runServlet(java.lang.String, fi.iki.elonen.NanoHTTPD.Method, java.util.Map,
   *      java.util.Map, java.util.Map, java.io.File)
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {
    if (params.containsKey(ClientConstants.REQUEST_MESSAGE_PARAM)) {
      try {
        logger.info("received: {}", params.get(ClientConstants.REQUEST_MESSAGE_PARAM));
        if (JSONUtils.validateSchema(this.schema, params.get(ClientConstants.REQUEST_MESSAGE_PARAM))) {
          return new Response(Response.Status.OK, VVoteWebServer.MIME_JSON, this.processMessage(
              params.get(ClientConstants.REQUEST_MESSAGE_PARAM)).toString());
        }
        else {
          return new Response(Response.Status.BAD_REQUEST, NanoHTTPD.MIME_PLAINTEXT, "Message validation failed");
        }
      }

      catch (NoSuchAlgorithmException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }

      catch (JSONException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }

      catch (TVSSignatureException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (JSONIOException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }
      catch (TVSKeyStoreException e) {
        logger.error("Exception whilst processing message", e);
        return new Response(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message");
      }

    }
    else {
      return new Response(Response.Status.BAD_REQUEST, NanoHTTPD.MIME_PLAINTEXT, "Missing msg parameter - no message to process");
    }
  }
}
